#!/usr/bin/env python
import os
import json
from itertools import chain, repeat

from pathlib import Path
from shutil import copy

from fake_useragent import UserAgent

from networkx import DiGraph
from networkx.readwrite.json_graph import node_link_data, node_link_graph

from onegram import Login
from onegram import user_info, following, followers
from onegram.utils import repeat


GRAPHS_DIR = Path('graphs')
username = os.getenv('INSTA_USERNAME')


def pull(depth=0):
    query_chunks = {
        'following': repeat(1000),
        'followers': repeat(1000),
        'posts': repeat(100),
        'feed': repeat(100),
        'likes': repeat(100),
        'comments': repeat(100),
        'explore': repeat(100),
    }
    settings = {
        'QUERY_CHUNKS': query_chunks,
        'USER_AGENT': UserAgent().random
    }
    graph = load()
    with Login(custom_settings=settings):
        crawl(username, graph, depth)

    dump(graph)
    return graph


def crawl(username, graph, depth):
    if not graph.nodes.get(username):
        user = user_info(username)
        graph.add_node(username, **user)
        dump(graph)
    else:
        user = graph.nodes[username]

    if depth:
        # Following
        user_following = list(following(user))
        prev_following = set(graph.successors(username))
        now_following = set(u['username'] for u in user_following)

        begin_following = now_following - prev_following
        end_following = prev_following - now_following
        following_changed = begin_following or end_following

        if following_changed:
            graph.remove_edges_from(zip(repeat(username), prev_following))
            graph.add_edges_from(zip(repeat(username), now_following))
            dump(graph)

        # Followers
        user_followers = list(followers(user))
        prev_followers = set(graph.predecessors(username))
        now_followers = set(u['username'] for u in user_followers)

        begin_followers = now_followers - prev_followers
        end_followers = prev_followers - now_followers
        followers_changed = begin_followers or end_followers

        if followers_changed:
            graph.remove_edges_from(zip(prev_followers, repeat(username)))
            graph.add_edges_from(zip(now_followers, repeat(username)))
            dump(graph)

        for f in chain(now_following, now_followers):
            crawl(f, graph, depth-1)


def load():
    path = GRAPHS_DIR / f'{username}.json'
    if path.exists():
        copy(str(path), str(path.with_suffix('.json~')))
        with open(path) as f:
            return node_link_graph(json.load(f))
    else:
        return DiGraph()


def dump(graph):
    GRAPHS_DIR.mkdir(parents=True, exist_ok=True)
    path = GRAPHS_DIR / f'{username}.json'
    with open(path, 'w') as f:
        json.dump(node_link_data(graph), f)


if __name__ == "__main__":
    pull(1)
