from onegram import Login
from tools import get_settings
from algorithms import likers_rank


with Login(custom_settings=get_settings()):
    rank = likers_rank('<user>')

