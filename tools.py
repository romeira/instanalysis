from fake_useragent import UserAgent

from onegram.utils import repeat


def get_settings():
    rate_limits = {
        '*': [(1, 1)],
        'actions': [(1, 2)],
    }

    query_chunks = {
        'following': repeat(1000),
        'followers': repeat(1000),
        'posts': repeat(100),
        'feed': repeat(100),
        'likes': repeat(100),
        'comments': repeat(100),
        'explore': repeat(100),
    }

    settings = {
        'RATE_LIMITS': rate_limits,
        'QUERY_CHUNKS': query_chunks,
        'USER_AGENT': UserAgent().random,
    }

    return settings
